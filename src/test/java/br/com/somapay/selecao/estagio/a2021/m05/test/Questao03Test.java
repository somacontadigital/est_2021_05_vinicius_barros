package br.com.somapay.selecao.estagio.a2021.m05.test;

import java.util.Arrays;

import br.com.somapay.selecao.estagio.a2021.m05.Questao03;
import junit.framework.TestCase;

public class Questao03Test extends TestCase {

	public void test01() {
		assertEquals(imprimir(new Integer[]{1, 1, 1, 3, 5}), imprimir(new Questao03().tribonacci(new Integer[]{1, 1, 1}, 5)));
	}

	public void test02() {
		assertEquals(imprimir(new Integer[]{0, 0, 1, 1, 2, 4}), imprimir(new Questao03().tribonacci(new Integer[]{0, 0, 1}, 6)));
	}

	public void test03() {
		assertEquals(imprimir(new Integer[]{10}), imprimir(new Questao03().tribonacci(new Integer[]{10, 10, 11}, 1)));
	}

	public void test04() {
		assertEquals(imprimir(new Integer[]{}), imprimir(new Questao03().tribonacci(new Integer[]{100, 100, 11}, 0)));
	}

	public void testAleatorio() {
		Integer[] arrayEntrada = new Integer[]{1, 1, 1};
		Integer[] arraySaida = new Integer[]{1, 1, 1, 3, 5, 9, 17, 31, 57, 105};
		for(int i = 1; i <= 100; i++) {
			assertEquals(imprimir(multiplica(arraySaida, i)), imprimir(new Questao03().tribonacci(multiplica(arrayEntrada, i), 10)));
		}
	}

	private Integer[] multiplica(Integer[] array, int i) {
		Integer[] novoArray = Arrays.copyOf(array, array.length);
		for(int j = 0; j < array.length; j++) novoArray[j] *= i;
		return novoArray;
	}
	
	private String imprimir(Integer[] array) {
		if(array.length == 0)
			return "";
		
		String retorno = "";
		for(int j = 0; j < array.length; j++) 
			retorno += array[j] + ",";
		retorno = retorno.substring(0, retorno.length() - 1);
		return retorno;
	}
}
