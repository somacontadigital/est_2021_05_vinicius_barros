package br.com.somapay.selecao.estagio.a2021.m05;

public class Questao03 {

	/**
	 * Crie uma função que a partir de 3 números seja capaz de gerar um
	 * sequência de N números, onde o 4º elemento seja a soma dos três
	 * primeiros, o 5º a soma do 2º, 3º e 4º e assim sucessivamente. Esta função
	 * deve receber dois parâmetros, e retornar um array com todos os N números
	 * da sequência
	 * 
	 * Exemplos:
	 * 		-> tribonacci([1, 1, 1], 5) = [1, 1, 1, 3, 5] 
	 * 		-> tribonacci([0, 0, 1], 6) = [0, 0, 1, 1, 2, 4] 
	 * 		-> tribonacci([10, 10, 11], 1) = [10] 
	 * 		-> tribonacci([100, 100, 11], 0) = [] 
	 * 
	 * @param sequenciaInicial
	 *            - 3 primeiros números da sequência
	 *            
	 * @param tamanhoSequenciaFinal
	 *            - numero de elementos que deve ser retornados. Este número
	 *            deve ser maior ou igual a 0. Se for passado 0 deve ser
	 *            retornado um array vazio.
	 *            
	 * @return - array com tamanhoSequenciaFinal elementos
	 */
	public Integer[] tribonacci(Integer[] sequenciaInicial, Integer tamanhoSequenciaFinal) {
		Integer[] sequenciaFinal = new Integer[tamanhoSequenciaFinal];
		for(int i = 0; i<sequenciaFinal.length;i++) {
			if(sequenciaInicial.length > i) {
				sequenciaFinal[i] = sequenciaInicial[i];
			}else {
				sequenciaFinal[i] = sequenciaFinal[i-1] + sequenciaFinal[i-2] + sequenciaFinal[i-3];	
			}
		}
		return sequenciaFinal;
	}

	public static void main(String[] args) {
		// TODO se quiser debugar ou fazer algum teste
	}
}
